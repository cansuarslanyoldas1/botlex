package com.hib.learning;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.hib.learning.library.Employee;

public class Main {
	
    public static void main(String[] args) {
        
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        //Database'e insert atıldığını kontrol et
        Employee employee = new Employee();
        employee.setId(1L);
        employee.setName("CansuYoldas");
        employee.setDepartment("Software");
        session.save(employee);

      
        session.getTransaction().commit();
        session.close();
        sessionFactory.close();
    }
}
